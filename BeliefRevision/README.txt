Belief Revision README File.
Made by
Victor Foged   - s174267
Mikkel Lehmann - s153048
Pedro Germani  - s190050

The project is made in Java, as such you will need to have Java installed 
to run the program.
In the zip folder we have the complete Eclipse project attached, 
along with a runable Jar file. 
The jar file can be run from the console using the command 'java -jar KB.jar'.

OBS: When writing formula, remember to seperate all tokens/terminals with a space.
Like so: ( A OR B ) IF ( NOT B AND C )

Quick Guide to the code:
The implementation of the logic related algorithms is mainly in the KB.java class.
The parser and the main class is located in the BeliefRev.java class. 
The Expression class and it's subclasses are for the Abstract Syntax Tree generated