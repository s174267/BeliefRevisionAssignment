
public class Atomic extends Expression {
	boolean value;
	String symbol;
	
	public Atomic(String sym, boolean val) {
		this.symbol = sym;
		this.value = val;
	}
	public Atomic(String sym) {
		this.symbol = sym;
	}

	@Override
	public boolean eval() {
		
		return value;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return symbol;
	}
	
}
