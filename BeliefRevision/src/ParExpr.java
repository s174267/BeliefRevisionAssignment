
public class ParExpr extends Expression {

	
	public ParExpr(Expression ex) {
		this.leftChild = ex;
	}
	@Override
	public boolean eval() {
		return (leftChild.eval());
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "(" + this.leftChild.toString() + ")";
	}

}
