import java.util.ArrayList;

public class Sentence implements Comparable<Sentence> {
	
	Expression root;
	ArrayList<Atomic> literals = new ArrayList<Atomic>();
	int rank;
	
	public Sentence(Expression r) {
		this.root = r;
		getLiterals(r);
	}
	public Sentence(Expression r, int rank) {
		this.root = r;
		getLiterals(r);
		this.rank = rank;
	}
	
	public void getLiterals(Expression node){
		
		if(node instanceof Atomic) {
			Atomic lit = (Atomic) node;
			literals.add(lit);
		}
		else if(node instanceof ParExpr) {
			getLiterals(node.leftChild);
		}
		else if(node instanceof NotExpr) {
			getLiterals(node.leftChild);
		}
		else {
			getLiterals(node.leftChild);
			getLiterals(node.rightChild);
		} 
		
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return root.toString();
	}
	@Override
	public int compareTo(Sentence o) {
		if(this.rank > o.rank) {
			return 1;
		}
		else if(this.rank < o.rank) {
			return -1;
		}
		else {
			return 0;
		}
		
	}
	
	
	
	
//	public Sentence(String sentence) {
//		String[] symbols = sentence.split(" ");
//		ArrayList<Expression> exprList = new ArrayList<Expression>();
//		ArrayList<Atomic> literals = new ArrayList<Atomic>();
//		
//		for (int i = 0; i < symbols.length; i++) { //Get Literals and Values
//			if(symbols[i] != "&" || symbols[i] != "|") {
//				boolean run = true;
//				while(run) {
//					System.out.println(symbols[i]);
//					String val = System.console().readLine();
//					if(val == "true") {
//						literals.add(new Atomic(symbols[i], true));
//						run = false;
//					}
//					else if(val == "false") {
//						literals.add(new Atomic(symbols[i], false));
//						run = false;
//					}
//				}
//			}
//			
//		}
//		
//		
//		
//		
//		
//		
//	}
//	
//	public Atomic getLiteral(String s, ArrayList<Atomic> list) {
//		for (Atomic lit : list) {
//			if(lit.symbol == s) {
//				return lit;
//			}
//		}
//		return null;
//	}
}
