import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Stack;

public class BeliefRev {

	public static void main(String[] args) throws IOException {
		KB kb = new KB();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Options:\n1. Add Sentence to KB \n2. Check Entailment of a Sentence \n3. Show KB \n4. Get valid models for the KB \n5. Remove a specific sentence from the KB\nType 'end' to quit");
		String input = reader.readLine();
		while(!input.equals("end")) {
			
				if(input.equals("1")) {
					System.out.println("Enter a sentence to add to the KB: \nTokens: NOT, AND, OR, IF, IFF, (, )\nEach token should be seperated by a space");
					String sentence = reader.readLine();
					System.out.println("Type in a priority ranking (higher = more important)");
					int rank = Integer.parseInt(reader.readLine());
					Sentence s = new Sentence(translateInput(sentence.split(" "), kb), rank);
					System.out.println("Adding sentence: " + s.toString());
					kb.addSentence(s);
				}
				if(input.equals("2")) {
					System.out.println("Enter a sentence to add to the KB: \nTokens: NOT, AND, OR, IF, IFF, (, )\nEach token should be seperated by a space");
					String sentence = reader.readLine();
					Sentence s = new Sentence(translateInput(sentence.split(" "), kb));
					if(kb.entails(s)) {
						System.out.println("Sentence is entailed by the KB");
					}
					else {
						System.out.println("Sentence is not entailed by the KB");
					}
				}
				if(input.equals("3")) {
					System.out.println("The KB contains the following sentences:");
					for (Sentence s : kb.KB) {
						System.out.println(s);
					}
				}
				if(input.equals("4")) {
					if(kb.KB.size() > 0 ) {
						System.out.println(kb.getValidStates(kb.KB).toString());
					}
					else {
						System.out.println("KB is empty");
					}
				}
				if(input.equals("5")) {
					System.out.println("The KB contains the following sentences:");
					int index = 1;
					for (Sentence s : kb.KB) {
						System.out.println(index + ". " + s);
						index++;
					}
					System.out.println("Type which sentence number you want to delete");
					int toRemove = Integer.parseInt(reader.readLine());
					kb.KB.remove(toRemove-1);
					
				}
				System.out.println("Options:\n1. Add Sentence to KB \n2. Check Entailment of a Sentence \n3. Show KB \n4. Get valid models for the KB \n5. Remove a specific sentence from the KB\nType 'end' to quit");
				input = reader.readLine();
		}
		
	}
	
	public static Expression translateInput(String[] input, KB kb) {
		String[] chars = input;
		HashMap<Integer, Expression> predicate = new HashMap<Integer, Expression>();
		Expression root = null;

		Stack<Integer> parStarts = new Stack<Integer>();
		for (int i = 0; i < chars.length; i++) {
			if(chars[i].equals("(")) {
				parStarts.push(i);
			}
			if(chars[i].equals(")")) {
				int startPar = parStarts.pop();
				String[] par = Arrays.copyOfRange(chars, startPar+1, i);
				Expression exp = translateInput(par, kb);
				ParExpr parExp = new ParExpr(exp);
				for (int j = startPar; j <= i; j++) {
					predicate.put(j, parExp);
					chars[j] = "null";
				}
				root = parExp;
			}
		}
		
		for (int i = 0; i < chars.length; i++) {
			if(chars[i] != "NOT" && chars[i] != "OR" && chars[i] != "AND" && chars[i] != "null" && chars[i] != "(" && chars[i] != ")" && chars[i] != "IF" && chars[i] != "IFF") {
				Atomic atom = null;	
				for (Atomic atomic : kb.literals) {
					if(atomic.symbol.equals(chars[i])) {
						
						atom = atomic;
					}
				}
				if(atom == null) {
					atom = new Atomic(chars[i]);
					kb.literals.add(atom);
				}
						
				predicate.put(i, atom);
				root = atom;
			}
		}
		
		for (int i = 0; i < chars.length; i++) {
			if(chars[i].equals("NOT")) {
				NotExpr newNot = new NotExpr(predicate.get(i + 1));
				predicate.put(i, newNot);
				for (Entry<Integer, Expression> set : predicate.entrySet()) {
					if(set.getValue().equals(newNot.leftChild)) {
						predicate.put(set.getKey(), newNot);
					}
				}	
				root = newNot;
			}
		}
		
		for (int i = 0; i < chars.length; i++) {
			if(chars[i].equals("AND")) {
				AndExpr newAND = new AndExpr(predicate.get(i - 1), predicate.get(i + 1));
				predicate.put(i, newAND);
				for (Entry<Integer, Expression> set : predicate.entrySet()) {
					if(set.getValue().equals(newAND.rightChild)) {
						predicate.put(set.getKey(), newAND);
					}
					if(set.getValue().equals(newAND.leftChild)) {
						predicate.put(set.getKey(), newAND);
					}
				}
				root = newAND;
			}
		}
		
		for (int i = 0; i < chars.length; i++) {
			if(chars[i].equals("OR")) {
				OrExpr newOR = new OrExpr(predicate.get(i - 1), predicate.get(i + 1));
				predicate.put(i, newOR);
				for (Entry<Integer, Expression> set : predicate.entrySet()) {
					if(set.getValue().equals(newOR.rightChild)) {
						predicate.put(set.getKey(), newOR);
					}
					if(set.getValue().equals(newOR.leftChild)) {
						predicate.put(set.getKey(), newOR);
					}
				}	
				root = newOR;
			}
		}
		for (int i = 0; i < chars.length; i++) {
			if(chars[i].equals("IF")) {
				IfExpr newIF = new IfExpr(predicate.get(i - 1), predicate.get(i + 1));
				predicate.put(i, newIF);
				for (Entry<Integer, Expression> set : predicate.entrySet()) {
					if(set.getValue().equals(newIF.rightChild)) {
						predicate.put(set.getKey(), newIF);
					}
					if(set.getValue().equals(newIF.leftChild)) {
						predicate.put(set.getKey(), newIF);
					}
				}	
				root = newIF;
			}
		}
		
		for (int i = 0; i < chars.length; i++) {
			if(chars[i].equals("IFF")) {
				IffExpr newIff = new IffExpr(predicate.get(i - 1), predicate.get(i + 1));
				predicate.put(i, newIff);
				for (Entry<Integer, Expression> set : predicate.entrySet()) {
					if(set.getValue().equals(newIff.rightChild)) {
						predicate.put(set.getKey(), newIff);
					}
					if(set.getValue().equals(newIff.leftChild)) {
						predicate.put(set.getKey(), newIff);
					}
				}	
				root = newIff;
			}
		}
		return root;
		
		
	}
	
	
	
}
