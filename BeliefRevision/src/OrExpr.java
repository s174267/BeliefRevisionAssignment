
public class OrExpr extends Expression {
	
	
	public OrExpr(Expression l, Expression r) {
		this.leftChild = l;
		this.rightChild = r;
	}

	@Override
	public boolean eval() {
		
		return leftChild.eval() || rightChild.eval();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.leftChild.toString() + " OR " + this.rightChild.toString();
	}

}
