import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class KB {
	
	ArrayList<Sentence> KB = new ArrayList<Sentence>();
	ArrayList<Atomic> literals;
	public KB() {
		literals = getLiterals();	
	}
	
	public boolean entails(Sentence s) {
		HashSet<HashMap<Atomic, Boolean>> kbSet = getValidStates(KB);
		ArrayList<Sentence> sent = new ArrayList<Sentence>();
		sent.add(s);
		HashSet<HashMap<Atomic, Boolean>> sSet = getValidStates(sent);
		return sSet.containsAll(kbSet);
		
	}
	
	public void recreateKB(Sentence s) {
		KB.add(s);
		literals = getLiterals();
		if(this.getValidStates(KB).isEmpty()) {
			System.out.println("Removed: " + s.toString());
			KB.remove(s);
		}
	}
	public void addSentence(Sentence s) {
		
		if(KB.size() == 0 || !this.entails(s)) {
			KB.add(s);
			literals = getLiterals();
			if(this.getValidStates(KB).isEmpty()) {
				contractKB(s);
			}
		}
		else {
			System.out.println("Sentence \"" + s.toString() +"\" is already entailed by the KB, it has not been added");
		}
		
	}
	
	public void contractKB(Sentence s) {
		KB newKB = new KB();
		newKB.addSentence(s);
		
		KB.remove(s);
		Collections.sort(KB);
		Collections.reverse(KB);
		for (Sentence sentence : KB) {
			newKB.recreateKB(sentence);
		}
		
		this.KB = newKB.KB;
		this.literals = newKB.literals;
		
	}
	public HashSet<HashMap<Atomic, Boolean>> unionOfSets(HashSet<HashMap<Atomic, Boolean>> set1, HashSet<HashMap<Atomic, Boolean>> set2){
		set1.addAll(set2);
		return set1;
	}
	
	public ArrayList<Atomic> addValElement(ArrayList<Atomic> model, Atomic sym, boolean val){
		sym.value =val;
		model.add(sym);
		return new ArrayList<Atomic>(model);
	}
	
	public HashSet<HashMap<Atomic, Boolean>> ttCheckAll(ArrayList<Sentence> KB, ArrayList<Atomic> symbols, ArrayList<Atomic> model){
		if(symbols.size() == 0) {
			ArrayList<Boolean> truths = new ArrayList<Boolean>();
			HashSet<HashMap<Atomic, Boolean>> valid = new HashSet<HashMap<Atomic, Boolean>>();
			for (Sentence sentence : KB) {
				truths.add(sentence.root.eval());
			}
			if(!truths.contains(false)) {
				HashMap<Atomic, Boolean> newMap = new HashMap<Atomic, Boolean>();
				for (int j = 0; j < literals.size(); j++) {
					newMap.put(literals.get(j), literals.get(j).value);
				}

				valid.add(newMap);
				
			}
			return valid;
		}
		else {
			Atomic sym = symbols.get(0);
			symbols.remove(0);	
			return unionOfSets(ttCheckAll(KB, new ArrayList<Atomic>(symbols), addValElement(model, sym, true)), ttCheckAll(KB, new ArrayList<Atomic>(symbols), addValElement(model, sym, false)));
		}
		
	}
	
	
	public HashSet<HashMap<Atomic, Boolean>> getValidStates(ArrayList<Sentence> kb) {
		
		return ttCheckAll(kb, new ArrayList<Atomic>(literals), new ArrayList<Atomic>());
		
	}
	public  ArrayList<Atomic> getLiterals(){
		LinkedHashSet<Atomic> literals = new LinkedHashSet<Atomic>();
		for (Sentence s : this.KB) {
			literals.addAll(s.literals);
		}
		
		return new ArrayList<Atomic>(literals);
	}

}
