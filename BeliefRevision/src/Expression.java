
public abstract class Expression {
	
	public Expression leftChild;
	public Expression rightChild;
	
	public abstract boolean eval();

}
