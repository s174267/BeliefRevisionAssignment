
public class IfExpr extends Expression {

	
	public IfExpr(Expression l, Expression r) {
		this.leftChild = l;
		this.rightChild = r;
	}
	@Override
	public boolean eval() {
		if(leftChild.eval() == true && rightChild.eval() == false) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public String toString() {
		// TODO Auto-generated method stub
		return this.leftChild.toString() + " -> " + this.rightChild.toString();
	}

}
