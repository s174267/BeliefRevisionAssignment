
public class IffExpr extends Expression {
	
	
	public IffExpr(Expression l, Expression r) {
		this.leftChild = l;
		this.rightChild = r;
	}
	@Override
	public boolean eval() {
		if(leftChild.eval() == rightChild.eval()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public String toString() {
		// TODO Auto-generated method stub
		return this.leftChild.toString() + " <-> " + this.rightChild.toString();
	}
}
