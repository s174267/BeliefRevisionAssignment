
public class NotExpr extends Expression {

	
	
	public NotExpr(Expression ex) {
		this.leftChild = ex;
	}
	@Override
	public boolean eval() {
		
		return !(leftChild.eval());
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "NOT " + this.leftChild.toString();
	}

}
